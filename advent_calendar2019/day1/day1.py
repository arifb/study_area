import numpy as np

filepath = "./input"
total_fuel = 0

#first part
print("First part...")
with open(filepath) as fp:
	for cnt, line in enumerate(fp):
		module_fuel = np.floor(int(line)/3) - 2
		total_fuel += module_fuel
		#print(f"Module mass: {line} - fuel needed: {module_fuel}")

print("Total fuel: ", total_fuel)


#second part
print("Second part...")
def calculate_fuel(mass):
	fuel = np.floor(int(mass)/3) - 2
	if(fuel > 0):
		return fuel
	else:
		return 0



all_fuel = 0
with open(filepath) as fp:
	for cnt, line in enumerate(fp):
		total_fuel = 0
		fuel = np.floor(int(line)/3) - 2
		total_fuel = fuel
		while(fuel > 0):
			fuel = calculate_fuel(fuel)
			total_fuel += fuel
		#print(f"Module mass: {line} - fuel needed: {total_fuel}")
		all_fuel += total_fuel

print("All fuel: ", all_fuel)
